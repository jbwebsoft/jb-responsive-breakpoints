# jb_responsive_breakpoints

An Angular Component to provide media-query events in javascript to other Angular Components

#IMPORTANT!!!

Test functionality of this component without Chrome device emulation resize. 
JavaScript matchMedia seems not to fire events in Chrome device emulation mode.
